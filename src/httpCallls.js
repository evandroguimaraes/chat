import axios from 'axios' ;

export function getMessages() {
    axios.get('http://localhost:3000/chat')
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    });
}

export function sendMessage(message) {

    let bodyString = "{ \"content\": \"Exemplo\" }";
    let body = JSON.parse(bodyString);
    console.log(body);
    axios.post('http://localhost:3000/message', body)
    .then(function (response) {
        console.log(response);
        //update the chat box with the response
        //updateChatBox(response.data); 
    })
    .catch(function (error) {
        console.log(error);
    });
}

export default getMessages;