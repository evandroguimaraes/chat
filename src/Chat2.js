import React from 'react';
import { Component } from 'react';
import axios from 'axios';
import './Chat.scss';

class Chat extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sender: "",
            password: 0.0,
            messages: [],
            message: ''
        }

        this.interval = setInterval(() => {
            this.getMessages();
        }, 5000);  
    }

    refreshState(messages) {
        this.setState({ messages: messages });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.sender !== this.state.sender) {
            this.getMessages();
        }
    }
      

    getUser(password) {
        axios.get('http://localhost:3000/user/' + password) 
            .then((response) => {
                let intendedRecipient = "Juh";
                const sender = response.data.name;
                if (sender === "Juh") {
                    intendedRecipient = prompt("Com quem você vai conversar?");
                }
                this.setState({ sender: response.data.name, intentedRecipient: intendedRecipient});
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getMessages() {
        axios.get('http://localhost:3000/chat/' + this.state.sender + "/" + this.state.intentedRecipient)
            .then((response) => {
                this.updateChat(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    sendMessage(message) {

        const msgContent = document.getElementById('message-to-send');

        const body = {
            "content": msgContent.value,
            "sender": this.state.sender,
            "password": this.state.password,
            "intendedRecipient": this.state.intentedRecipient
        }

        msgContent.value = '';
        msgContent.focus();

        axios.post('http://localhost:3000/message', body)
            .then((response) => {
                this.updateChat(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    updateChat(response) {
        const localstate = Object.assign({}, this.state);

        localstate.messages = response.data.sort((a, b) => {
            return a.messageNumber - b.messageNumber;
        });

        localstate.messages = response.data;
        this.setState(localstate);
    }

    /*
    componentDidMount() {
        this.getMessages();
    }
    */
    handleSubmit() {
        this.sendMessage(document.getElementById('message-to-send').value);
    }

    header() {
        return (
            <div className="chat-header clearfix">
                <div>
                    <img src={require('./images/Juh.jpg')} alt="avatar" className="avatar"/> 
                </div>
            <div className="chat-about">
                <div className="chat-with">Chat da Juh</div>
                <div className="chat-num-messages"></div>
            </div>
                <i className="fa fa-star"></i>
            </div> 
        );
    }

    myMessage(message) {
        return (
            <li className="clearfix">
                <div className="message-data align-right">
                    <span className="message-data-time" >{message.messageTime}</span> &nbsp; &nbsp;
                    <span className="message-data-name" >{message.sender}</span> <i className="fa fa-circle me"></i>
                </div>
                <div className="message other-message float-right">
                    {message.content}
                </div>
            </li> 
        );
    }

    otherMessage(message) {
        return (
            <li>
                <div className="message-data">
                    <span className="message-data-name"><i className="fa fa-circle online"></i>{message.sender}</span>
                    <span className="message-data-time">{message.messageTime}</span>
                </div>
                <div className="message my-message">
                    {message.content}
                </div>
            </li>            
        );
    }

    render() { 

        if (this.state.sender === "") {
            let password = prompt("Informe a sua senha");
            this.getUser(password);
            return;
        }

        return (
            <div className="container clearfix">
                <div className="chat">
                    {this.header()}
              
                    <div className="chat-history">
                        <ul>
                            {this.state.messages.slice(0).reverse().map((item, id) => {
                                return ( item.sender === this.state.sender ? this.myMessage(item) : this.otherMessage(item));
                            })}
                        </ul>                
                    </div> 
              
                    <div className="chat-message clearfix">
                        <textarea name="message-to-send" id="message-to-send" placeholder ="Digite a sua mensagem" rows="3"></textarea>
                        <i className="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;
                        <i className="fa fa-file-image-o"></i>
                        <button onClick={() => {this.sendMessage();}}>Send</button>
                    </div> 
                </div> 
            </div> 
        );
    }
}

export default Chat;