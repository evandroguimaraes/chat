import React from 'react';
import { Component } from 'react';
import axios from 'axios' ;
import './App.css';

class Chat extends Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            message: ''
        }

        this.interval = setInterval(() => {
            this.getMessages();
        }, 5000);    
    }

    refreshState(messages) {
        this.setState({messages: messages});
    }

    getMessages() {
        axios.get('http://localhost:3000/chat')
        .then( (response) => {
            this.updateChat(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    sendMessage(message) {

        const msgContent = document.getElementById('message-content');

        const body = {
            "content":              msgContent.value,
            "sender":               document.getElementById('message-sender').value,
            "password":             document.getElementById('message-password').value,
            "intendedRecipient":    document.getElementById('message-intended-recipient').value
        }

        msgContent.value = '';
        msgContent.focus();        

        console.log("body: " + JSON.stringify(body));

        axios.post('http://localhost:3000/message', body)
        .then((response) => {
            this.updateChat(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    updateChat(response) {
        const localstate = Object.assign({}, this.state);
        localstate.messages = response.data;
        this.setState(localstate);
    }
    
    componentDidMount() {
        this.getMessages();
    }

    handleSubmit() {
        this.sendMessage(document.getElementById('message-to-send').value);
    }

    render() {

        return (
            <div className="chat color-cdb4db">

                <div className="chat-box-title">
                    <div className="chat-message color-ffafcc">
                        Chat da Juh
                    </div>
                </div>

                <div className="chat-box-top">
                    {   Object.values(this.state.messages).map((value) => {
                            return value.map((item) => {
                                return <div className="chat-message">{item.content}<div className="time-right">{item.time}</div></div>
                            })
                        })
                    }
                </div>
            
                <div className="chat-box-bottom">
                    <div className="chat-message">
                        <div className="chat-footer">                        
                            <input type="text"
                                className="text-height border-text-color"
                                id="message-sender" 
                                placeholder="Digite o seu nome" />
                            <input type="password"  
                                className="text-height border-text-color"
                                id="message-password"
                                placeholder="Informe uma senha" />
                            <input type="text"
                                className="text-height border-text-color"
                                id="message-intended-recipient"
                                placeholder="Digite o nome do destinatário" />
                        </div>
                        <div className="chat-send">
                            <textarea 
                                className="text-height border-text-color"  
                                id="message-content" 
                                placeholder="Type your message..." />
                            <button 
                                className="button-send text-height border-text-color"
                                onClick={() => { 
                                    this.sendMessage();
                                }}>Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Chat;